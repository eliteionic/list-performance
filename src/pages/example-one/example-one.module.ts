import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExampleOnePage } from './example-one';

@NgModule({
  declarations: [
    ExampleOnePage,
  ],
  imports: [
    IonicPageModule.forChild(ExampleOnePage),
  ],
})
export class ExampleOnePageModule {}
