import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-example-one',
  templateUrl: 'example-one.html',
})
export class ExampleOnePage {

	items: any[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams) {

	}

	ionViewDidLoad() {

		this.items = new Array(300);

	}

	doInfinite(infiniteScroll){
		
		for(let i=0; i<20; i++){
			this.items.push('');
		}

		infiniteScroll.complete();

	}

}
